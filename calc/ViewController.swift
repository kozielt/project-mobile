import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!;
	var operation:Int = 0;
	var oldValue:String = "";
    
    @IBAction func numbers(_ sender: UIButton) {
		label.text = label.text! + String(sender.tag - 1)
    }
	
	@IBAction func clear() {
		label.text = "";
		operation = 0;
		oldValue = "";
	}
	
	@IBAction func addSign(_ sender: UIButton) {
		if hasMinusSign() {
			label.text!.remove(at: label.text!.startIndex)
		} else {
			label.text = "-" + label.text!
		}
	}
	
	@IBAction func countProcent() {
		let number = parseCurrentNum();
		label.text = String(number / 100);
	}
	
	func hasMinusSign() -> Bool {
		if let text = label.text {
			return text.hasPrefix("-");
		} else {
			return false;
		}
	}
	
	@IBAction func add(_ sender: Any) {
		if let text = label.text {
			if text != "" {
				oldValue = text;
				operation = 1;
				label.text = "";
			}
		}
	}
	
	@IBAction func minus() {
		if let text = label.text {
			if text != "" {
				oldValue = text;
				operation = 2;
				label.text = "";
			}
		}
	}
	
	@IBAction func multiplication() {
		if let text = label.text {
			if text != "" {
				oldValue = text;
				operation = 3;
				label.text = "";
			}
		}
	}
    
    @IBAction func divide() {
        if let text = label.text {
            if text != "" {
                oldValue = text;
                operation = 4;
                label.text = "";
            }
        }
    }
    
	@IBAction func sum() {
		if operation == 0 {
			return;
		} else {
			let current = parseCurrentNum();
			doMath(current);
			operation = 0;
		}
	}
	
	func doMath(_ current: Double) {
		if operation == 1 {
			label.text = String(current + (Double(oldValue) ?? 0));
		} else if operation == 2 {
			label.text = String((Double(oldValue) ?? 0) - current);
		} else if operation == 3 {
			label.text = String((Double(oldValue) ?? 1) * current);
        } else if operation == 4 {
            label.text = String((Double(oldValue) ?? 0) / current);
        }
	}
	
	@IBAction func gimmeDot() {
		if let text = label.text {
			if text.range(of: ".") == nil{
				label.text!.append(".");
			}
		}
	}
	
	func parseCurrentNum() -> Double {
		if let text = label.text {
			if let number = Double(text) {
				return number;
			}
		}
		return 0.0;
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
